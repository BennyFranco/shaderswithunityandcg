﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class DecalOnOff : MonoBehaviour
{
    Material material;
    bool showDecal;
    // Start is called before the first frame update
    void Start()
    {
        material = this.GetComponent<Renderer>().sharedMaterial;
    }

    void OnMouseDown()
    {
        showDecal = !showDecal;

        if(showDecal)
        {
            material.SetFloat("_ShowDecal", 1);
        }
        else
        {
            material.SetFloat("_ShowDecal", 0);
        }
    }
}
