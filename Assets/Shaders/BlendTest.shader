﻿Shader "ErgoDon/BlendTest"
{
    Properties
    {
        _MainTex ("Albedo (RGB)", 2D) = "black" {}
    }
    SubShader
    {
        Tags { "Queue"="Transparent" }
        // One -> The Value of One - use this to let either the source or the destination color come through fully.
        Blend One One

        // Gets the alpha from current texture and multiply by 1 minus current alpha
        // Tradicional Blend for transparency
        // Blend SrcAlpha OneMinusSrcAlpha

        // Current color in framebuffer will be multiplying by zero, so that color will be discarded
        // Soft additive blend
        // Blend DstColor Zero
        Pass
        {
            SetTexture [_MainTex] { combine texture }
        }
    }
    FallBack "Diffuse"
}
