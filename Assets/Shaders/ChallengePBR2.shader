﻿Shader "ErgoDon/ChallengePBR2"
{
    Properties
    {
        _Color ("Color", Color) = (1,1,1,1)
        _SpecularTex("Specular Texture (R)", 2D) = "white" {}
        _SpecColor("Specular", Color) = (1,1,1,1)
    }
    SubShader
    {
        Tags { "Queue"="Geometry" }
        
        CGPROGRAM
        #pragma surface surf StandardSpecular

        sampler2D _SpecularTex;
        fixed4 _Color;

        struct Input
        {
            float2 uv_SpecularTex;
        };

        // SurfaceOutputStandard works with Standard light and allow us to access to metallic and smoothness values
        void surf (Input IN, inout SurfaceOutputStandardSpecular o)
        {
            o.Albedo = _Color.rgb;
            o.Smoothness = 0.9-tex2D(_SpecularTex, IN.uv_SpecularTex).r;
            o.Specular = _SpecColor.rgb;
        }
        ENDCG
    }
    FallBack "Diffuse"
}
