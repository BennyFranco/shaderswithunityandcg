﻿Shader "ErgoDon/HelloShader" 
{
	
	// Properties section of the shader
	Properties
	{
		_myColor ("Example Color", Color) = (1,1,1,1)
		_myEmission("Example Emmission", Color) = (1,1,1,1)
		_myNormal("Example Normal", Color) = (1,1,1,1)
	}


	// Shader logic
	SubShader
	{
		CGPROGRAM
			// This pragma tells to unity how you want the code to be used, first this 
			// indicates a "surface" keyword, that means this is a surface shader
			// then indicates the name of the function to be called, in this case
			// we create and call the function named "surf" and finally
			// the ligthing model
			#pragma surface surf Lambert

			struct Input 
			{
				float2 uvMainTex;
			};

			fixed4 _myColor;
			fixed4 _myEmission;
			fixed4 _myNormal;

			void surf(Input IN, inout SurfaceOutput o) 
			{
				o.Albedo = _myColor.rgb;
				o.Emission = _myEmission.rgb;
				o.Normal = _myNormal.rgb;
			}

		ENDCG
	}

	// Fallback in case of incompatible shader logic with old GPU
	Fallback "Diffuse"
}