﻿Shader "ErgoDon/PropertiesChallenge2"
{
    Properties
    {
        _myTex("Example Texture", 2D) = "white" {}
    }
    SubShader
    {
        CGPROGRAM
        #pragma surface surf Lambert

        sampler2D _myTex;

        struct Input 
        {
            // The name of this must be uv or uv2 followed of the name of the texture, in this case "_myTex"
            float2 uv_myTex;
        };

        void surf (Input IN, inout SurfaceOutput o)
        {
            o.Albedo = (tex2D(_myTex, IN.uv_myTex)).rgb;
            o.Albedo.g = 1;
        }

        ENDCG
    }
    FallBack "Diffuse"
}
