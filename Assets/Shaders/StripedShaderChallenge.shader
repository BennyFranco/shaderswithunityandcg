﻿Shader "ErgoDon/StripedShaderChallenge"
{
    Properties
    {
        _RimColor("Rim Color", Color) = (0, 0.5, 0.5, 0.0)
        _RimPower("Rim Power", Range(0.5,8.0)) = 3.0
        _StripeWidth("StripeWidth", Range(1,20)) = 10
        _DiffuseTex("Diffuse Texture", 2D) = "white" {}
    }
    SubShader
    {
        CGPROGRAM
        #pragma surface surf Lambert

        struct Input
        {
            float2 viewDir;
            float3 worldPos;
            float2 uv_DiffuseTex;
        };

        float4 _RimColor;
        float _RimPower;
        float _StripeWidth;
        sampler2D _DiffuseTex;

        void surf (Input IN, inout SurfaceOutput o)
        {
            o.Albedo = tex2D(_DiffuseTex, IN.uv_DiffuseTex).rgb;

            half rim = (1 - saturate(dot(normalize(IN.viewDir), o.Normal))) * _RimPower;
            o.Emission = frac(IN.worldPos.y*_StripeWidth) > 0.4 ? float3(0,1,0)*rim : float3(1,0,0)*rim;
        }
        ENDCG
    }
    FallBack "Diffuse"
}
