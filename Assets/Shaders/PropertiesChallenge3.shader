﻿Shader "ErgoDon/PropertiesChallenge3"
{
    Properties
    {
        _myTex("Example Texture", 2D) = "white" {}
    }
    SubShader
    {
        CGPROGRAM
        #pragma surface surf Lambert

        sampler2D _myTex;

        struct Input 
        {
            // The name of this must be uv or uv2 followed of the name of the texture, in this case "_myTex"
            float2 uv_myTex;
        };

        void surf (Input IN, inout SurfaceOutput o)
        {
            float4 green = float4(0,1,0,1);
            o.Albedo = (tex2D(_myTex, IN.uv_myTex) * green).rgb;
        }

        ENDCG
    }
    FallBack "Diffuse"
}
