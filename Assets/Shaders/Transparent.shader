﻿Shader "ErgoDon/Transparent"
{
    Properties
    {
        _MainTex ("Albedo (RGB)", 2D) = "black" {}
    }
    SubShader
    {
        Tags { "Queue"="Transparent" }

        // Gets the alpha from current texture and multiply by 1 minus current alpha
        // Tradicional Blend for transparency
        Blend SrcAlpha OneMinusSrcAlpha

        // Culling off 
        Cull Off
        
        Pass
        {
            SetTexture [_MainTex] { combine texture }
        }
    }
    FallBack "Diffuse"
}
