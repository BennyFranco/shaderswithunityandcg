﻿Shader "ErgoDon/PropertiesChallenge4"
{
    Properties
    {
        _diffuseTex("Diffuse Texture", 2D) = "white" {}
        _emissionTex("Emission Texture", 2D) = "white" {}
    }
    SubShader
    {
        CGPROGRAM
        #pragma surface surf Lambert

        sampler2D _diffuseTex;
        sampler2D _emissionTex;

        struct Input 
        {
            // The name of this must be uv or uv2 followed of the name of the texture, in this case "_myTex"
            float2 uv_diffuseTex;
            float2 uv_emissionTex;
        };

        void surf (Input IN, inout SurfaceOutput o)
        {
            o.Albedo = tex2D(_diffuseTex, IN.uv_diffuseTex).rgb;
            o.Emission = tex2D(_emissionTex, IN.uv_emissionTex).rgb;
        }

        ENDCG
    }
    FallBack "Diffuse"
}
